import React, { Component } from 'react';
import * as d3 from 'd3';
import './App.css';

class Chart extends Component {
  componentDidMount() {
    const { chartId, data } = this.props;
    d3.select(`#${chartId}`)
      .selectAll('div')
      .data(data)
      .enter()
      .append('div')
      .style('width', d => `${d}px`)
      .text(d => `${d}`);
  }

  render() {
    const { chartId } = this.props;
    return (
      <div className='chart' id={chartId} />
    );
  }
}

export default Chart;
