import React, { Component } from 'react';
import Chart from './Chart';
import './App.css';

class App extends Component {
  state = { display: 'none', canShow: true };

  buttonClick(text) {
    if (this.state.canShow) {
      this.setState({
        canShow: false,
        display: 'block',
        text
      });
    }
  }

  closeClick() {
    this.setState({
      canShow: true,
      display: 'none'
    });
  }

  renderPopUp() {
    const { modalStyle, closeStyle, modalContent } = styles;
    const { display, text } = this.state;
    return (
      <div style={{...modalStyle, ...{ display }}}>
        <div style={modalContent}>
          <span
            style={closeStyle}
            onClick={this.closeClick.bind(this)}
          >
            &times;
          </span>
          <p>{text}</p>
        </div>
      </div>
    );
  }

  renderExcersizeOne() {
    const { display } = this.state;
    return (
      <div>
        {display === 'block' ? this.renderPopUp() : null}
        <button onClick={this.buttonClick.bind(this, 'Első gomb')}>
          Első gomb
        </button>
        <button onClick={this.buttonClick.bind(this, 'Második gomb')}>
          Második gomb
        </button>
        <button onClick={this.buttonClick.bind(this, 'Harmadik gomb')}>
          Harmadik gomb
        </button>
      </div>
    );
  }

  renderExcersizeTwo() {
    return (
      <div>
        <Chart chartId='first' data={[30, 86, 168, 281, 303, 365]} />
      </div>
    );
  }

  render() {
    return (
      <div className='App'>
        {this.renderExcersizeOne()}
        {this.renderExcersizeTwo()}
      </div>
    );
  }
}

const styles = {
  modalStyle: {
    position: 'fixed',
    zIndex: 1,
    paddingTop: 100,
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  modalContent: {
    backgroundColor: '#fefefe',
    margin: 'auto',
    padding: 20,
    border: '1px solid #888',
    width: '80%'
  },
  closeStyle: {
    color: '#aaaaaa',
    float: 'right',
    fontSize: 28,
    fontWeight: 'bold'
  }
};

export default App;
